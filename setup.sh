mkdir ~/myproject
cd ~/myproject

cp ~/docean-flask-gunicorn-nginx/*.py .

sudo cp ~/docean-flask-gunicorn-nginx/myproject.conf /etc/init/myproject.conf

sudo cp ~/docean-flask-gunicorn-nginx/myproject.ngx /etc/nginx/sites-available/myproject
sudo ln -s /etc/nginx/sites-available/myproject /etc/nginx/sites-enabled
